package com.manjarrez.courseregistration.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.manjarrez.courseregistration.models.Instructor;

public interface InstructorRepository extends PagingAndSortingRepository<Instructor, Long> {

	List<Instructor> findByDepartmentDepartmentKeyOrderByNameLastAscNameFirst(String key);
}
