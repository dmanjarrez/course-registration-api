package com.manjarrez.courseregistration.models;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.tomcat.util.file.Matcher;

@Entity
@Table(name = "instructor")
public class Instructor extends DomainEntity {

	private String instructorIdNumber;
	
	@Embedded
	private Name name;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "department_id")
	private Department department;

	protected Instructor() { }
	public Instructor(Name name, Department department) {
		setName(name);
		setDepartment(department);
	}

	public String getInstructorIdNumber() {
		return instructorIdNumber;
	}

	public void setInstructorIdNumber(String instructorId) {
		if(Matcher.match("\\d{10}", instructorId, false))
			throw new IllegalArgumentException("Instructor ID number must be exactly 10 digits.");

		this.instructorIdNumber = instructorId;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		if(name == null)
			throw new IllegalArgumentException("Instructors name is required.");

		this.name = name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		if(department == null)
			throw new IllegalArgumentException("Instructor department is required.");
		
		this.department = department;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		
		if(o == null || o.getClass() != this.getClass())
			return false;
		
		Instructor instructor = (Instructor)o;
		
		return instructor.instructorIdNumber == this.instructorIdNumber;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(instructorIdNumber);
	}
	
}
