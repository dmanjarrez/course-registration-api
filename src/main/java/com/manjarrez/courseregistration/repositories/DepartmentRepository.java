package com.manjarrez.courseregistration.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.manjarrez.courseregistration.models.Department;

public interface DepartmentRepository extends PagingAndSortingRepository<Department, Long> {

}
