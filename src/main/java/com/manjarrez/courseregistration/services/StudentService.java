package com.manjarrez.courseregistration.services;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.Student;

@Service
public interface StudentService {

	Page<Student> list(Integer pageNumber, Integer pageSize);
	
	Student get(long id);
	
	void addStudent(Student student); 
}
