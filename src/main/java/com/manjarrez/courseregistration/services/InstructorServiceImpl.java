package com.manjarrez.courseregistration.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.Instructor;
import com.manjarrez.courseregistration.repositories.InstructorRepository;

@Service
public class InstructorServiceImpl implements InstructorService {

	@Autowired
	private InstructorRepository instructorRepository;
	
	@Override
	public List<Instructor> findByDepartmentKey(String key) {
		return instructorRepository.findByDepartmentDepartmentKeyOrderByNameLastAscNameFirst(key);
	}
	
	@Override
	public Instructor get(long id) {
		return instructorRepository.findById(id).get();
	}
	

}
