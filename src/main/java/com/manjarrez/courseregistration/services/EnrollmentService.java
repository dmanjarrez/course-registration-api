package com.manjarrez.courseregistration.services;

import com.manjarrez.courseregistration.models.EnrollmentRequest;
import com.manjarrez.courseregistration.models.EnrollmentResponse;

public interface EnrollmentService {
	
	EnrollmentResponse RequestEnrollment(EnrollmentRequest request);
	
	EnrollmentResponse RequestDisenrollment(EnrollmentRequest request);

}
