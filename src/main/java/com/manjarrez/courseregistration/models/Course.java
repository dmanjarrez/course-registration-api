package com.manjarrez.courseregistration.models;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.manjarrez.courseregistration.helpers.StringHelper;

@Entity
@Table(name = "course")
public class Course extends DomainEntity {
	
	@ManyToOne
	@JoinColumn(name = "departmentId")
	private Department department;

	private int number;
	private String name;
	private String description;
	private int capacity;
	
	protected Course() { }

	public Course(Department department, int number, String name, String description, int capacity) {
		if(department == null ||  StringHelper.isEmpty(name) || StringHelper.isEmpty(description) || capacity < 1)
			throw new IllegalArgumentException("Courses require a department, a number, a name, a description, and a capacity greater than zero.");
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		
		if(o == null || o.getClass() != this.getClass())
			return false;
		
		Course course = (Course)o;
		
		return course.department.equals(this.department) 
				&& course.number == this.number;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(department, number);
	}
}
