package com.manjarrez.courseregistration.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "section")
public class Section extends DomainEntity {
	
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course;

	private int sectionNumber;
	
	@ManyToOne
	@JoinColumn(name = "instructor_id")
	private Instructor instructor;

	@ManyToMany(cascade = {CascadeType.MERGE})
	@JoinTable(
			name = "enrollments", 
			joinColumns = @JoinColumn(name = "section_id"), 
			inverseJoinColumns = @JoinColumn(name = "student_id"))
	private List<Student> students;
	
	@ManyToOne
	@JoinColumn(name = "semester_id")
	private Semester semester;

	protected Section() { }

	public Section(Course course, int number, Semester semester) {
		setCourse(course);
		setSectionNumber(number);
		setSemester(semester);
		students = new ArrayList<Student>();
	}

	public int getClassSize() {
		return students.size();
	}
	
	public boolean isFull() {
		return students.size() == course.getCapacity();
	}
	
	public void enrollStudent(Student student) {
		if(isFull())
			throw new IllegalStateException("Cannot enroll student: Section is full.");
		
		students.add(student);
		student.getSections().add(this);
	}
	
	public void disenrollStudent(Student student) {
		students.remove(student);
		student.getSections().remove(this);
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		if(course == null)
			throw new IllegalArgumentException("Section course is required.");
		
		this.course = course;
	}

	public int getSectionNumber() {
		return sectionNumber;
	}

	public void setSectionNumber(int number) {
		this.sectionNumber = number;
	}

	public Instructor getInstructor() {
		return instructor;
	}

	public void setInstructor(Instructor instructor) {
		this.instructor = instructor;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		if(students == null)
			throw new IllegalArgumentException("Students cannot be null.");
		
		this.students = students;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		if(semester == null)
			throw new IllegalArgumentException("Section must have a semester.");
		
		this.semester = semester;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		
		if(o == null || o.getClass() != this.getClass())
			return false;
		
		Section section = (Section)o;
		
		return section.course.equals(this.course) 
				&& section.sectionNumber == this.sectionNumber
				&& section.semester.equals(this.semester);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(course, sectionNumber, semester);
	}

}
