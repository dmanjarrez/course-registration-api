package com.manjarrez.courseregistration.models;

import javax.persistence.Embeddable;
import com.manjarrez.courseregistration.helpers.StringHelper;

@Embeddable
public class Name {
	private String first;
	private String middle;
	private String last;

	protected Name() { }

	public Name(String first, String middle, String last) {
		setFirst(first);
		setMiddle(middle);
		setLast(last);
	}
	
	public Name(String first, String last) {
		setFirst(first);
		setLast(last);
	}

	
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		if(StringHelper.isEmpty(first)) 
			throw new IllegalArgumentException("First name is required.");

		this.first = first;
	}
	public String getMiddle() {
		return middle;
	}
	public void setMiddle(String middle) {
		this.middle = middle;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		if(StringHelper.isEmpty(last)) 
			throw new IllegalArgumentException("Last name is required.");

		this.last = last;
	}
}
