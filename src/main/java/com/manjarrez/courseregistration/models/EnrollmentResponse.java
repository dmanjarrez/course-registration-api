package com.manjarrez.courseregistration.models;

public class EnrollmentResponse {

	private boolean enrollmentSuccessful;
	private String message;

	public EnrollmentResponse(boolean successfull, String message) {
		this.enrollmentSuccessful = successfull;
		this.message = message;
	}
	
	public boolean isEnrollmentSuccessful() {
		return enrollmentSuccessful;
	}

	public void setEnrollmentSuccessful(boolean enrollmentSuccessful) {
		this.enrollmentSuccessful = enrollmentSuccessful;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	} 
}
