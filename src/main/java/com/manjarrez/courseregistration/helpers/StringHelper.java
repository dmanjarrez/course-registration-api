package com.manjarrez.courseregistration.helpers;

public final class StringHelper {

	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}
}
