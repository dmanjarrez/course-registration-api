package com.manjarrez.courseregistration.models;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "semester")
public class Semester extends DomainEntity {
	private int year;
	
	@Enumerated(EnumType.STRING)
	private Term term;

	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Term getTerm() {
		return term;
	}
	public void setTerm(Term term) {
		this.term = term;
	}
@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		
		if(o == null || o.getClass() != this.getClass())
			return false;
		
		Semester semester = (Semester)o;
		
		return semester.year == this.year && semester.term == this.term;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(year, term);
	}
}
