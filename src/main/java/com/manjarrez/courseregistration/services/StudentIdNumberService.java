package com.manjarrez.courseregistration.services;

public interface StudentIdNumberService {

	String createNewStudentIdNumber();
	boolean addStudentIdNumber(long entityId, String studentIdNumber);
	String getBusIdByEntityId(long entityId);
	long getEntityIdByBusId(String studentIdNumber);
}
