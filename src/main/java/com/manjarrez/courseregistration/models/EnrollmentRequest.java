package com.manjarrez.courseregistration.models;

public class EnrollmentRequest {
	private long sectionId;
	private long studentId;

	public long getSectionId() {
		return sectionId;
	}

	public void setSectionId(long sectionId) {
		this.sectionId = sectionId;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	
}
