package com.manjarrez.courseregistration.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.Student;
import com.manjarrez.courseregistration.repositories.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {
	private final int DEF_PAGE_SIZE = 10;
	private final int DEF_PAGE_NUMBER = 0;

	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private StudentIdNumberService studentIdNumberService;
	
	@Override
	public Page<Student> list(Integer pageNumber, Integer pageSize) {
		int actualPageSize = pageSize == null || pageSize < 1 ? DEF_PAGE_SIZE : pageSize;
		int actualPageNumber = pageNumber == null || pageNumber < 1 ? DEF_PAGE_NUMBER : pageNumber;
		System.out.println(actualPageSize);
		System.out.println(actualPageNumber);
		
		return studentRepository.findAll(PageRequest.of(
				actualPageNumber, 
				actualPageSize, 
				Sort.by("name.last", "name.first")));							
	}
	
	@Override
	public Student get(long id) {
		return studentRepository.findById(id).get();
	}

	@Override
	public void addStudent(Student student) {
		student.setStudentIdNumber(studentIdNumberService.createNewStudentIdNumber());
		studentRepository.save(student);
		studentIdNumberService.addStudentIdNumber(student.getId(), student.getStudentIdNumber());
	}
}
