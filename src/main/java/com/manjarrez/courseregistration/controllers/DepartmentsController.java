package com.manjarrez.courseregistration.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.manjarrez.courseregistration.models.Course;
import com.manjarrez.courseregistration.models.Department;
import com.manjarrez.courseregistration.models.Section;
import com.manjarrez.courseregistration.services.DepartmentService;

@RestController
@RequestMapping("/api/v1/departments")
public class DepartmentsController {
	
	@Autowired
	private DepartmentService departmentService;
	
	@GetMapping
	public List<Department> list() {
		
		return departmentService.list();
	}
	
	@GetMapping("/{id}")
	public Department get(@PathVariable("id") long id) {
		return departmentService.get(id);
	}
	
	@GetMapping("/{key}/courses")
	public Page<Course> getCourses(
			@PathVariable("key") String departmentKey,
			@RequestParam(value = "pageSize", required = false) Integer pageSize, 
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber) {
		
		return departmentService.getCourses(departmentKey, pageNumber, pageSize);
	}
	
	@GetMapping("/{key}/courses/{number}/sections")
	public List<Section> getCourseSections(
			@PathVariable("key") String departmentKey,
			@PathVariable("number") int courseNumber) {
		
		return departmentService.getCourseSections(departmentKey, courseNumber);
	}
}
