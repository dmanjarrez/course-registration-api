package com.manjarrez.courseregistration.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.manjarrez.courseregistration.models.EnrollmentRequest;
import com.manjarrez.courseregistration.models.EnrollmentResponse;
import com.manjarrez.courseregistration.services.EnrollmentService;

@RestController
@RequestMapping("/api/v1/enrollments")
public class EnrollmentsController {
	
	@Autowired
	private EnrollmentService enrollmentService;
	
	@PostMapping("/enroll/request")
	@ResponseStatus(HttpStatus.CREATED)
	public EnrollmentResponse enroll(@RequestBody EnrollmentRequest enrollmentRequest) {
		
		return enrollmentService.RequestEnrollment(enrollmentRequest);
		
	}

	@PostMapping("/disenroll/request")
	@ResponseStatus(HttpStatus.CREATED)
	public EnrollmentResponse disenroll(@RequestBody EnrollmentRequest enrollmentRequest) {
		
		return enrollmentService.RequestDisenrollment(enrollmentRequest);
		
	}
}
