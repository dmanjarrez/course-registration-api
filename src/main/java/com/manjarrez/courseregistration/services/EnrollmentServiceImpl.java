package com.manjarrez.courseregistration.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.EnrollmentRequest;
import com.manjarrez.courseregistration.models.EnrollmentResponse;
import com.manjarrez.courseregistration.models.Section;
import com.manjarrez.courseregistration.models.Student;
import com.manjarrez.courseregistration.repositories.SectionRepository;
import com.manjarrez.courseregistration.repositories.StudentRepository;

@Service
public class EnrollmentServiceImpl implements EnrollmentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private SectionRepository sectionRepository;
	
	
	@Override
	public EnrollmentResponse RequestEnrollment(EnrollmentRequest request) {
		Optional<Section> section = sectionRepository.findById(request.getSectionId());
		Optional<Student> student = studentRepository.findById(request.getStudentId());

		if(!student.isPresent())
			return new EnrollmentResponse(false, "Student does not exist.");
		
		if(!section.isPresent())
			return new EnrollmentResponse(false, "Section does not exist.");
		
		if(!studentIsEnrolledInSection(request.getStudentId(), section.get()))
			return new EnrollmentResponse(false, "Student is already enrolled in this section.");
		
		if(section.get().isFull())
			return new EnrollmentResponse(false, "Section is full.");
		
		section.get().getStudents().add(student.get());
		sectionRepository.save(section.get());
		
		return new EnrollmentResponse(true, "Student enrolled.");
	}

	@Override
	public EnrollmentResponse RequestDisenrollment(EnrollmentRequest request) {
		Optional<Section> section = sectionRepository.findById(request.getSectionId());
		Optional<Student> student = studentRepository.findById(request.getStudentId());

		if(!student.isPresent())
			return new EnrollmentResponse(false, "Student does not exist.");
		
		if(!section.isPresent())
			return new EnrollmentResponse(false, "Section does not exist.");
		
		if(!studentIsEnrolledInSection(request.getStudentId(), section.get()))
			return new EnrollmentResponse(false, "Student is not enrolled in this section.");
		
		section.get().getStudents().remove(student.get());
		sectionRepository.save(section.get());
		
		return new EnrollmentResponse(true, "Student disenrolled.");

	}

	private boolean studentIsEnrolledInSection(long studentId, Section section) {
		return section.getStudents().stream().allMatch(s -> s.getId() == studentId);
	}
}
