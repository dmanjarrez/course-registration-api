package com.manjarrez.courseregistration.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manjarrez.courseregistration.models.Instructor;
import com.manjarrez.courseregistration.services.InstructorService;

@RestController
@RequestMapping("/api/v1/instructors")
public class InstructorsController {
	
	@Autowired
	private InstructorService instructorService;
	
	@GetMapping("/{id}")
	public Instructor get(@PathVariable("id") long id) {
		return instructorService.get(id);
	}
	
	@GetMapping("/department/{departmentKey}")
	public List<Instructor> getByDepartment(@PathVariable("departmentKey") String key) {
		return instructorService.findByDepartmentKey(key);
	}
}
