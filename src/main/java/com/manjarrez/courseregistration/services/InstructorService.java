package com.manjarrez.courseregistration.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.Instructor;

@Service
public interface InstructorService {

	Instructor get(long id);

	List<Instructor> findByDepartmentKey(String key); 
}
