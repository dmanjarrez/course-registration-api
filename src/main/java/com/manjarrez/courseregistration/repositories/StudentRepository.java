package com.manjarrez.courseregistration.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.manjarrez.courseregistration.models.Student;

public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {

}
