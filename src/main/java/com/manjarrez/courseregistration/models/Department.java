package com.manjarrez.courseregistration.models;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.manjarrez.courseregistration.helpers.StringHelper;

@Entity
@Table(name = "department")
public class Department extends DomainEntity {
	private String departmentKey;
	private String name;
	private String abbreviation;
	
	protected Department() { }

	public Department(String departmentKey, String name, String abbreviation) {
		setDepartmentKey(departmentKey);
		setName(name);
		setAbbreviation(abbreviation);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(StringHelper.isEmpty(name))
			throw new IllegalArgumentException("Department name is required.");
		
		this.name = name;
	}
	
	public void setAbbreviation(String abbreviation) {
		if(StringHelper.isEmpty(name))
			throw new IllegalArgumentException("Department abbreviation is required.");
	}
	
	public String getAbbreviation() {
		return abbreviation;
	}

	public String getDepartmentKey() {
		return departmentKey;
	}

	public void setDepartmentKey(String departmentKey) {
		if(StringHelper.isEmpty(departmentKey))
			throw new IllegalArgumentException("Department key is required.");
		
		this.departmentKey = departmentKey;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		
		if(o == null || o.getClass() != this.getClass())
			return false;
		
		Department department = (Department)o;
		
		return department.departmentKey == this.departmentKey; 
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(departmentKey);
	}
}