package com.manjarrez.courseregistration.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.manjarrez.courseregistration.models.Section;

public interface SectionRepository extends PagingAndSortingRepository<Section, Long> {

	List<Section> findByCourseDepartmentDepartmentKeyAndCourseNumber(String departmentKey, int courseNumber);
}
