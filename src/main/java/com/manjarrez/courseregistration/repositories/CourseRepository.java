package com.manjarrez.courseregistration.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.manjarrez.courseregistration.models.Course;

public interface CourseRepository extends PagingAndSortingRepository<Course, Long> {

	Page<Course> findByDepartmentDepartmentKeyOrderByNumber(String key, Pageable page);
}
