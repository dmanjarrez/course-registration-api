package com.manjarrez.courseregistration.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.Course;
import com.manjarrez.courseregistration.models.Department;
import com.manjarrez.courseregistration.models.Section;
import com.manjarrez.courseregistration.repositories.CourseRepository;
import com.manjarrez.courseregistration.repositories.DepartmentRepository;
import com.manjarrez.courseregistration.repositories.SectionRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	private final int DEF_PAGE_SIZE = 10;
	private final int DEF_PAGE_NUMBER = 0;
	
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private SectionRepository sectionRepository;
	
	@Override
	public List<Department> list() {
		
		List<Department> depts = new ArrayList<Department>();
		departmentRepository.findAll(Sort.by("name")).forEach(d -> depts.add(d));

		return depts;
	}
	
	@Override
	public Department get(long id) {
		return departmentRepository.findById(id).get();
	}
	
	@Override
	public Page<Course> getCourses(String departmentKey, Integer pageNumber, Integer pageSize) {
		int actualPageSize = pageSize == null || pageSize < 1 ? DEF_PAGE_SIZE : pageSize;
		int actualPageNumber = pageNumber == null || pageNumber < 1 ? DEF_PAGE_NUMBER : pageNumber;
		
		return courseRepository.findByDepartmentDepartmentKeyOrderByNumber(departmentKey, PageRequest.of(actualPageNumber, actualPageSize));
	}
	
	@Override
	public List<Section> getCourseSections(String departmentKey, int courseNumber) {
		return sectionRepository.findByCourseDepartmentDepartmentKeyAndCourseNumber(departmentKey, courseNumber);
	}

}
