package com.manjarrez.courseregistration.models;

public enum Term {
	Fall,
	Spring,
	Summer
}
