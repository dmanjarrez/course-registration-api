package com.manjarrez.courseregistration.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.manjarrez.courseregistration.models.Student;
import com.manjarrez.courseregistration.services.StudentService;

@RestController
@RequestMapping("/api/v1/students")
public class StudentsController {
	
	@Autowired
	private StudentService studentService;
	
	@GetMapping
	public Page<Student> list(
			@RequestParam(value = "pageSize", required = false) Integer pageSize, 
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber) {
		
		return studentService.list(pageSize, pageNumber);
	}
	
	@GetMapping("/{id}")
	public Student get(@PathVariable("id") long id) {
		return studentService.get(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void post(@RequestBody Student student) {
		System.out.println(student.toString());
		studentService.addStudent(student);
	}
}
