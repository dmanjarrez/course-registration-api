package com.manjarrez.courseregistration.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class StudentIdNumberServiceImpl implements StudentIdNumberService {

	/*
	 * In a real system this should use some kind of in-memory caching/no-sql type
	 * solution like memcache or Redis or something. This is in memory, so of course
	 * it won't persist when the app is restarted. Also, the ID would probably be an
	 * intelligent key.
	 */

	private Map<String, Long> busIdToEntityId;
	private Map<Long, String> entityIdToBusId;

	private Random rand;

	public StudentIdNumberServiceImpl() {
		busIdToEntityId = new HashMap<String, Long>();
		entityIdToBusId = new HashMap<Long, String>();

		rand = new Random();
	}

	public String createNewStudentIdNumber() {
		String newId;
		do {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < 9; i++) {
				sb.append(rand.nextInt(9));
			}
			newId = sb.toString();
			sb.delete(0, sb.length());
		} while (this.busIdToEntityId.containsKey(newId));

		return newId;
	}

	public boolean addStudentIdNumber(long entityId, String studentIdNumber) {
		// Key is not already in use, and student is not assigned to other key
		if (!busIdToEntityId.containsKey(studentIdNumber) && !entityIdToBusId.containsKey(entityId)) {

			busIdToEntityId.put(studentIdNumber, entityId);
			entityIdToBusId.put(entityId, studentIdNumber);

			return true;
		}

		return false;
	}

	@Override
	public String getBusIdByEntityId(long entityId) {
		if(this.entityIdToBusId.containsKey(entityId))
				return this.entityIdToBusId.get(entityId);
		else
			return null;
	}

	@Override
	public long getEntityIdByBusId(String studentIdNumber) {
		if(this.busIdToEntityId.containsKey(studentIdNumber))
			return this.busIdToEntityId.get(studentIdNumber);
		else
			return 0;
	}
}
