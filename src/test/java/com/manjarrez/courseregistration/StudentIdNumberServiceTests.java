package com.manjarrez.courseregistration;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

import com.manjarrez.courseregistration.services.StudentIdNumberServiceImpl;


public class StudentIdNumberServiceTests {

	@Test
	public void createNewStudentIdNumber_CreatesTenDigitNumber() {

		// Arrange
		StudentIdNumberServiceImpl svc = new StudentIdNumberServiceImpl();
		
		// Act
		String idNumber = svc.createNewStudentIdNumber();
		
		// Assert
		Assert.assertTrue(Pattern.matches("\\d{9}", idNumber));
	}
	
	
	@Test
	public void addStudentNumber_will_not_addDuplicateBusinessKey() {
		// Arrange 
		StudentIdNumberServiceImpl svc = new StudentIdNumberServiceImpl();
		String studentId = svc.createNewStudentIdNumber();
		svc.addStudentIdNumber(1, studentId);
		
		// Act 
		boolean result = svc.addStudentIdNumber(2, studentId);
		
		// Assert
		Assert.assertFalse(result);
		
	}

	@Test
	public void addStudentNumber_will_addNonDuplicateBusinessKeyAndNonDuplicateEntityKey() {
		// Arrange 
		StudentIdNumberServiceImpl svc = new StudentIdNumberServiceImpl();
		String studentId1 = svc.createNewStudentIdNumber();
		String studentId2 = svc.createNewStudentIdNumber();
		svc.addStudentIdNumber(1, studentId1);
		
		// Act 
		boolean result = svc.addStudentIdNumber(2, studentId2);
		
		// Assert
		Assert.assertTrue(result);
		
	}

	@Test
	public void addStudentNumber_will_not_addDuplicateEntityKey() {
		// Arrange 
		StudentIdNumberServiceImpl svc = new StudentIdNumberServiceImpl();
		String studentId1 = svc.createNewStudentIdNumber();
		String studentId2 = svc.createNewStudentIdNumber();
		svc.addStudentIdNumber(1, studentId1);
		
		// Act 
		boolean result = svc.addStudentIdNumber(1, studentId2);
		
		// Assert
		Assert.assertFalse(result);
		
	}

}
