package com.manjarrez.courseregistration.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.apache.tomcat.util.file.Matcher;

@Entity
@Table(name = "student")
public class Student extends DomainEntity {

	private String studentIdNumber;
	
	@Embedded
	private Name name;
	
	@ManyToMany(mappedBy = "students")
	private List<Section> sections;

	public Student() {
		sections = new ArrayList<Section>();
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		if(name == null)
			throw new IllegalArgumentException("Student must have a name");
		
		this.name = name;
	}
	
	public String getStudentIdNumber() {
		return studentIdNumber;
	}

	public void setStudentIdNumber(String studentId) {
		if(Matcher.match("\\d{10}", studentId, false))
			throw new IllegalArgumentException("Student ID number must be exactly 10 digits.");

		this.studentIdNumber = studentId;
	}

	public List<Section> getSections() {
		return sections;
	}

	public void setSections(List<Section> sections) {
		if(sections == null)
			throw new IllegalArgumentException("Sections cannot be null.");
		
		this.sections = sections;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		
		if(o == null || o.getClass() != this.getClass())
			return false;
		
		Student student = (Student)o;
		
		return student.studentIdNumber == this.studentIdNumber;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(studentIdNumber);
	}
}
