package com.manjarrez.courseregistration.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.manjarrez.courseregistration.models.Course;
import com.manjarrez.courseregistration.models.Department;
import com.manjarrez.courseregistration.models.Section;

@Service
public interface DepartmentService {

	List<Department> list();
	
	Department get(long id);
	
	Page<Course> getCourses(String departmentKey, Integer pageNumber, Integer pageSize);
	
	List<Section> getCourseSections(String departmentKey, int courseNumber);
}
